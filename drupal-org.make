core = 7.x
api = 2

; Modules
projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][download][type] = git
projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
projects[ctools][download][branch] = 7.x-1.x

projects[context][type] = module
projects[context][subdir] = contrib
projects[context][download][type] = git
projects[context][download][url] = http://git.drupal.org/project/context.git
projects[context][download][branch] = 7.x-3.x

projects[elements][type] = module
projects[elements][subdir] = contrib
projects[elements][download][type] = git
projects[elements][download][url] = http://git.drupal.org/project/elements.git
projects[elements][download][branch] = 7.x-1.x

projects[fapi_validation][type] = module
projects[fapi_validation][subdir] = contrib
projects[fapi_validation][download][type] = git
projects[fapi_validation][download][url] = http://git.drupal.org/project/fapi_validation.git
projects[fapi_validation][download][branch] = 7.x-1.x

projects[features][type] = module
projects[features][subdir] = contrib
projects[features][download][type] = git
projects[features][download][url] = http://git.drupal.org/project/features.git
projects[features][download][branch] = 7.x-1.x

projects[field_validation][type] = module
projects[field_validation][subdir] = contrib
projects[field_validation][download][type] = git
projects[field_validation][download][url] = http://git.drupal.org/project/field_validation.git
projects[field_validation][download][branch] = 7.x-2.x

projects[ife][type] = module
projects[ife][subdir] = contrib
projects[ife][download][type] = git
projects[ife][download][url] = http://git.drupal.org/project/ife.git
projects[ife][download][branch] = 7.x-2.x

projects[subprofiles][type] = module
projects[subprofiles][subdir] = contrib
projects[subprofiles][download][type] = git
projects[subprofiles][download][url] = http://git.drupal.org/project/subprofiles.git
projects[subprofiles][download][branch] = 7.x-1.x

projects[transliteration][type] = module
projects[transliteration][subdir] = contrib
projects[transliteration][download][type] = git
projects[transliteration][download][url] = http://git.drupal.org/project/transliteration.git
projects[transliteration][download][branch] = 7.x-3.x

projects[webform][type] = module
projects[webform][subdir] = contrib
projects[webform][download][type] = git
projects[webform][download][url] = http://git.drupal.org/project/webform.git
;Webform dev version: 3.x or 4.x?
projects[webform][download][branch] = 7.x-3.x


projects[clientside_validation][type] = module
projects[clientside_validation][subdir] = attiks
projects[clientside_validation][download][type] = git
projects[clientside_validation][download][url] = http://git.drupal.org/project/clientside_validation.git
projects[clientside_validation][download][branch] = 7.x-1.x

projects[webform_ajax_page][type] = module
projects[webform_ajax_page][subdir] = attiks
projects[webform_ajax_page][download][type] = git
projects[webform_ajax_page][download][url] = http://git.drupal.org/project/webform_ajax_page.git
projects[webform_ajax_page][download][branch] = 7.x-1.x

projects[webform_draggable_list][type] = module
projects[webform_draggable_list][subdir] = attiks
projects[webform_draggable_list][download][type] = git
projects[webform_draggable_list][download][url] = http://git.drupal.org/project/webform_draggable_list.git
projects[webform_draggable_list][download][branch] = 7.x-1.x

projects[webform_html_textarea][type] = module
projects[webform_html_textarea][subdir] = attiks
projects[webform_html_textarea][download][type] = git
projects[webform_html_textarea][download][url] = http://git.drupal.org/project/webform_html_textarea.git
projects[webform_html_textarea][download][branch] = 7.x-1.x

projects[webform_multifile][type] = module
projects[webform_multifile][subdir] = attiks
projects[webform_multifile][download][type] = git
projects[webform_multifile][download][url] = http://git.drupal.org/project/webform_multifile.git
projects[webform_multifile][download][branch] = 7.x-1.x

projects[webform_table_element][type] = module
projects[webform_table_element][subdir] = attiks
projects[webform_table_element][download][type] = git
projects[webform_table_element][download][url] = http://git.drupal.org/project/webform_table_element.git
projects[webform_table_element][download][branch] = 7.x-1.x
